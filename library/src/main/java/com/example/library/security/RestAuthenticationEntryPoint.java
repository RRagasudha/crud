package com.example.library.security;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component()
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint{

    //public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) throws IOException, ServletException {
 /* AuthenticationException authenticationException;
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getOutputStream().println("{ \"error\": \"" + authenticationException.getMessage() + "\" }");
		// TODO Auto-generated method stub
		
	}*/
	//private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
       // logger.error(authException.getMessage(), authException);
    	response.setStatus(HttpStatus.FORBIDDEN.value());

    	 final Map<String, Object> mapBodyException = new HashMap<>() ;

         mapBodyException.put("error"    , "Error from AuthenticationEntryPoint") ;
         mapBodyException.put("message"  , "Wrong user credentilas") ;
         mapBodyException.put("exception", "My stack trace exception") ;
         mapBodyException.put("path"     , request.getServletPath()) ;
         mapBodyException.put("timestamp", (new Date()).getTime()) ;

         response.setContentType("application/json") ;
         response.setStatus(HttpServletResponse.SC_UNAUTHORIZED) ;

         final ObjectMapper mapper = new ObjectMapper() ;
         mapper.writeValue(response.getOutputStream(), mapBodyException) ;
     }
    }
