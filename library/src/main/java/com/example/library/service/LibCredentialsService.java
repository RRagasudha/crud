package com.example.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.model.LibCredentials;


@Service
public class LibCredentialsService {
	@Autowired
	private LibCredentialsRepo cr;
	
	public List<LibCredentials> listAll()
	{
		return cr.findAll();
	}
	
	
	public void save(LibCredentials lib) 
	{
		//lib.setLibrary(ls.get(lib.getId()));
		cr.save(lib);
	}
	
	public LibCredentials get(int id)
	{
		
		return cr.findById(id).get();
	}
   public  LibCredentials findByName(String username)
	{
		System.out.println(username);
		//LibCredentials lib
		System.out.println(cr.findByUsername(username));
		return cr.findByUsername(username);
	}
	
	

}
