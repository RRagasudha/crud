package com.example.library.security;




import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;



@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	DataSource dataSource;
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		/* auth.inMemoryAuthentication()
         .withUser("Ragasudha").password("{noop}user").roles("USER").and()
         .withUser("admin").password("{noop}admin").roles("ADMIN");*/

		 auth.jdbcAuthentication().dataSource(dataSource)
			.usersByUsernameQuery("select username, password,enabled from lib_credentials where username=?")
			.authoritiesByUsernameQuery("select username , role from lib_credentials where username=?");
		
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.cors();
        http.csrf().
        disable()
        
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            .antMatchers("/login").permitAll()
            .antMatchers("/create").permitAll()
            .antMatchers("/reg").permitAll()
            .antMatchers("/delete").hasRole("ADMIN")
            .anyRequest().authenticated()
            
          .and()
            .httpBasic()
            .and()
           .formLogin().failureHandler(authenticationFailureHandler())
            .and().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint());
    }
    
    public AuthenticationEntryPoint authenticationEntryPoint() {
		return new RestAuthenticationEntryPoint();
	}

	@Bean
    public AuthenticationFailureHandler authenticationFailureHandler()
    {
      return new RestAuthenticationFailureHandler();
    }
    @Bean public PasswordEncoder passwordEncoder() { 
        return NoOpPasswordEncoder.getInstance(); 
    }
   
   
    
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}