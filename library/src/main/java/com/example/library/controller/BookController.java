package com.example.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.model.Books;
import com.example.library.service.BookService;

@RestController
@CrossOrigin(origins = "*")
public class BookController {
	@Autowired
	private BookService bs;
	
	String status;
	LibraryController lc;

	
	 @GetMapping("/blist")
	    public List<Books> bdisplay()
	    {
		  return bs.listAll();
	    }

	   @PostMapping("/bcreate")
	   public void newBook(@RequestBody Books book) 
	   {
		 bs.save(book);
	   }

	  @DeleteMapping("/bdelete/{bid}")
		public void bdelete(@PathVariable(name = "bid") int bid)
	    {
		    bs.delete(bid);
		    System.out.println(bid);
		          
		}

	  @PostMapping("/issue/{bid}/{id}")
	    public String bupdate(@PathVariable(name = "bid") int bid,@PathVariable(name = "id") int id) {
		 Books book = bs.get(bid);
		 if(book.getId()==0)
		 {
		 book.setId(id);
		 bs.save(book);
		 status="yes";
		 
		 }
		 else status="no";
		
	  return status;
	  }
	  
	 

}
