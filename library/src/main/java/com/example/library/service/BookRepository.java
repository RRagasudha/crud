package com.example.library.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.library.model.Books;

public interface BookRepository extends JpaRepository<Books,Integer> {

}
