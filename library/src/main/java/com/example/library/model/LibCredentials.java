package com.example.library.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Value;

@Entity

public class LibCredentials {
	
	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String username;
	private String password;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean enabled;
	
	@Value("USER")
	private  String role;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id", referencedColumnName= "id")
	private Library library;

	public LibCredentials() {
		
	}
	
	public LibCredentials(int id, String username, String password, Library library) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.library = library;
	}

	public LibCredentials(int id, String username, String password) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
	}


	public LibCredentials(int id, Library library) {
		super();
		this.id = id;
		this.library = library;
	}



	public LibCredentials(int id) {
	
		this.id = id;
	}

	public LibCredentials(int id, String username, String password, String role) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public LibCredentials(int id, String username, String password, String role, Library library) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
		this.library = library;
	}


	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getRole() {
		return role;
	}



	public void setRole(String role) {
		this.role = role;
	}
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}
	
	

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}