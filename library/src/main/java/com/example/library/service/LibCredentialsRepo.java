package com.example.library.service;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.library.model.LibCredentials;

public interface LibCredentialsRepo extends JpaRepository<LibCredentials,Integer>{
LibCredentials findByUsername(String username);
}
