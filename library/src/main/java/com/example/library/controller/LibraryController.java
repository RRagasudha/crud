package com.example.library.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.exception.ItemNotFoundException;
import com.example.library.model.Books;
import com.example.library.model.LibCredentials;
import com.example.library.model.Library;
import com.example.library.service.BookService;
import com.example.library.service.LibCredentialsService;
import com.example.library.service.LibraryService;




@RestController
@CrossOrigin(origins = "*")
public class LibraryController 

{
	@Autowired
	private LibraryService ls;
	@Autowired
	private LibCredentialsService cs;
	
	int uid;
	String role;
	int mid;
	String status;
	
	
	    
	
	    @GetMapping(produces = "application/json")
		@RequestMapping({ "/login/{username}"})
		   public void validateLogin(@PathVariable String username)
	      {
		      System.out.println("logged in");
		      LibCredentials lib=cs.findByName(username);
		      System.out.println(lib.getRole());
		      role = lib.getRole();
		      mid=lib.getId();
	           System.out.println(role);
		}
	
	   @RequestMapping("/role")
	       public String role()
	      {
		     System.out.println(role);
		     return role;
	      }
 
	   
	   @GetMapping("/list")
         public List<Library> display()
         {
	       return ls.listAll();
	     }
	   
	   @GetMapping("/details")
       public Library loggedInMember()
       {
	       return ls.get(mid);
	     }
	   
	   @GetMapping("/member/{id}")
       public Library member(@PathVariable(name = "id") int id)
       {
		   Optional<Library> lib=ls.getL(id);
		   
		   if(!lib.isPresent()) 
			   throw new ItemNotFoundException();
		   Library library=lib.get();
		   return library;
	       
	     }
 
       @PostMapping("/create")
          public void newMember(@RequestBody Library lib) 
         {
           ls.save(lib);
	       uid=lib.getId(); 
         }
 
      @DeleteMapping("/delete/{id}")
	     public void delete(@PathVariable(name = "id") int id)
         {
	        ls.delete(id);
	        System.out.println(id);
	     }
 
      @PutMapping("/update/{id}")
         public void update(@RequestBody Library lib ,@PathVariable(name = "id") int id) 
        {
	       lib.setId(id);
	       ls.save(lib);
        }
 
      @PostMapping("/reg" )
     public void register(@RequestBody LibCredentials lib) 
      {
	     lib.setId(uid);
	     lib.setEnabled(true);
	     lib.setRole("user");
	    // System.out.println(uid);
	     cs.save(lib);
	 
	 }
 
 /*************************************************************************************/
	@Autowired
	private BookService bs;

   
  @GetMapping("return/{bid}")
  public String bookreturn(@PathVariable(name = "bid") int bid) 
  {
	Books book = bs.get(bid);
	if(mid==book.getId()||mid==1)
	   {
	      book.setId(0);
	      status="Returned";
	     // bc.issue="No";
	      bs.save(book);
	   }
	else
		status="Not issued to you";
	
	return status;
	
}



	

}
