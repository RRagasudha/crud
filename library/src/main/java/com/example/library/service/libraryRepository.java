package com.example.library.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.library.model.Library;

public interface libraryRepository extends JpaRepository<Library,Integer> {

}
