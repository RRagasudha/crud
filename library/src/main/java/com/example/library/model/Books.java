package com.example.library.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Books {
	
	@Id
	private int bid;
	private String bname;
	private String bauth;
	private int bcost;
	
	private int id;
	
	public Books() {
		
	}
	

	public Books(int id) {
		super();
		this.id = id;
	}


	public Books(int bid, String bname, String bauth, int bcost, int id) {
		super();
		this.bid = bid;
		this.bname = bname;
		this.bauth = bauth;
		this.bcost = bcost;
		this.id = id;
	}


	public Books(int bid, String bname, String bauth, int bcost) {
		super();
		this.bid = bid;
		this.bname = bname;
		this.bauth = bauth;
		this.bcost = bcost;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public String getBname() {
		return bname;
	}

	public void setBname(String bname) {
		this.bname = bname;
	}

	public String getBauth() {
		return bauth;
	}

	public void setBauth(String bauth) {
		this.bauth = bauth;
	}

	public int getBcost() {
		return bcost;
	}

	public void setBcost(int bcost) {
		this.bcost = bcost;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
	
	
	
	
	
	

}
