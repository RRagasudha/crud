package com.example.library.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Library {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private int age;
	private String photoid;
	private String contact;
	
	@OneToMany(mappedBy="id",cascade= CascadeType.DETACH)
	//@JoinColumn(name="id", referencedColumnName="id")
	private List<Books> books;
	
	public Library() {
		
	}

	public Library(int id, String name, int age, String photoid, String contact) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.photoid = photoid;
		this.contact = contact;
	}
	
	

	public Library(List<Books> books) {
		super();
		this.books = books;
	}

	public Library(int id, List<Books> books) {
		super();
		this.id = id;
		this.books = books;
	}

	public Library(int id, String name, int age, String photoid, String contact, List<Books> books) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.photoid = photoid;
		this.contact = contact;
		this.books = books;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhotoid() {
		return photoid;
	}

	public void setPhotoid(String photoid) {
		this.photoid = photoid;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public List<Books> getBooks() {
		return books;
	}

	public void setBooks(List<Books> books) {
		this.books = books;
	}
	
	
	
	
	

}
