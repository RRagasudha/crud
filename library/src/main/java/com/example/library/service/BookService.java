package com.example.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.model.Books;

@Service
public class BookService {
	
	@Autowired
	private BookRepository br;
	public List<Books> listAll()
	{
		return br.findAll();
	}
	
	
	public void save(Books book) 
	{
		br.save(book);
	}
	

	public Books get(int bid) 
	{
		return br.findById(bid).get();
		 
	}
	
	public void delete(int bid)
	{
		br.deleteById(bid);
	}
	
	

}
