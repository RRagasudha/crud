package com.example.library.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.model.LibCredentials;
import com.example.library.model.Library;

@Service
public class LibraryService {
	
	@Autowired
	private libraryRepository lr;
	@Autowired
	private LibCredentialsRepo cr;
	
	public List<Library> listAll()
	{
		return lr.findAll();
	}
	
	
	public void save(Library lib) 
	{
		lr.save(lib);
		LibCredentials l = new LibCredentials();
		l.setLibrary(lib);
		l.setId(lib.getId());
		cr.save(l);
	}
	public Library get(int id) 
	{
		return lr.findById(id).get();
		 
	}

	public Optional<Library> getL(int id) 
	{
		return lr.findById(id);
		 
	}
	
	public void delete(int id)
	{
		lr.deleteById(id);
	}
	
	/*public Library getBook(int bid)
	{
		Library lib=lr.findById(bid).get();
		lib.setId
	}
	*/
	


}
